# Optimisation de l'attribution des permanences LCDA

## Description

PermOpt est un ensemble de scripts Python permettant d'attribuer les permanences de la crèche Les Copains d'Abord (LCDA), en optimisant la participation de chaque famille.

Il s'agit de récupérer des données à partir d'un sondage doodle décrivant les permanences à faire sur le mois et les disponibilités de chaque famille, et de générer un tableau des permanences optimal. L'objectif est de rapprocher le compteur horaire de chaque famille au plus proche de zéro.

## Utilisation

Le script `souhait.py` récupère les données du doodle, le script `permopt.py` lance l'optimisation mathématique, et le script `permopt_gui.py` lance une interface graphique permettant de manipuler ces scripts et de visualiser les résultats.

Dans un terminal, se placer dans le répertoire des scripts et lancer :
```
$ python souhaits.py <poll_id>
$ python permopt.py <datfile_prefix>
```
où `<poll_id>` est l'identifiant doodle du sondage, et `<datfile_prefix>` le nom du fichier .dat généré à l'étape précédente (par exemple `perm_2016-11_dispo_<poll_id>`).


## Installation

Cloner ce dépôt dans le répertoire de votre choix (par exemple `~`) :

```
$ git clone https://framagit.org/mservillat/PermOpt.git
```

PermOpt nécessite **Python 3.5** ainsi que différents paquets :
* **matplotlib**: http://matplotlib.org/
* **pandas**: http://pandas.pydata.org/
* **pyyaml**: http://pyyaml.org/wiki/PyYAML
* **requests**: http://docs.python-requests.org/en/master/
* **babel**
* **tzlocal**
* **pillow**
* **pyutilib**
* **pyomo 4.4.1** : http://www.pyomo.org/

La plupart de ces paquets peuvent s'installer avec la commande `conda` (distribution Python Anaconda ou Miniconda) ou avec la commande `pip` de Python (`easy_install pip` si non disponible).
 
PermOpt a été developpé et testé uniquement sous OSX (10.10 et 10.9), avec l'environnement Miniconda3 (https://conda.io/miniconda.html). Les commandes suivantes permettent d'installer un environnement virtuel dédié à PermOpt :

```
$ conda create -n permopt python=3.5
$ source activate permopt
$ conda install matplotlib pandas pyyaml requests babel tzlocal pillow pyutilib
```

Le paquet pyomo doit être installé manuellement à partir du code source. Lancer les commandes suivantes (dans l'environnement virtuel créé ci-dessus) :

```
$ cd ~/PermOpt
$ unzip pyomo-4.4.1.zip
$ cd pyomo-4.4.1
$ python setup.py install
$ pyomo --version
```

La dernière commande doit afficher la version de Pyomo qui vient d'être installée.

## Installation de Baron sous OSX (voir la section "Optimisateur" ci-dessous)

PermOpt a été particulièrement testé avec Baron 16.5.11 dans sa version limitée. Celui-ci doit être copié dans un répertoire du PATH et renommé `baron` pour qu'il soit découvert par Pyomo, par exemple :

```
$ cd ~/PermOpt
$ cp baron-16.5.11-osx64 ~/miniconda3/envs/permopt/bin/baron
```

## Création d'un script de lancement pour OSX

Lancer l'_AppleScript_ _Editor_ (ou _Script_ _Editor_, l'éditeur de script) et copier le texte suivant :

```
tell application "Terminal"
	do script with command "source activate permopt;python ~/PermOpt/permopt_gui.py;exit"
end tell

```

Sauver ce script dans les Applications ou sur le Bureau pour un accès rapide.

## Optimisateur (_Solver_)

Ce programme nécessite l'installation d'un optimisateur mathématique, plus particulièrement un optimisateur quadratique MINLP (_mixed-integer nonlinear problems_). 
Pyomo permet de formaliser un problème d'optimisation mathématique, mais c'est ensuite cet optimisateur qui va chercher une solution.

#### Guide d'installation
https://software.sandia.gov/downloads/pub/pyomo/PyomoInstallGuide.html#Solvers

#### Projets open-source (sans optimisation quadratique...) :
* GLPK (GNU Linear Programming Kit) : http://www.gnu.org/software/glpk/
* CBC : https://projects.coin-or.org/Cbc

#### Projets open-source (avec optimisation quadratique) :
* Ipopt (ne gère pas les entiers...) : http://www.coin-or.org/download/binary/Ipopt/
* Bonmin : https://projects.coin-or.org/Bonmin
* Couenne : https://projects.coin-or.org/Couenne

#### Projets commerciaux (version gratuite limitée) :
* Baron (version limitée gratuite) : http://www.minlp.com/baron-downloads
* Gurobi Optimizer (licence académique possible) : http://www.gurobi.com/
* IBM ILOG CPLEX Optimizer (version limitée gratuite) : http://www-01.ibm.com/software/integration/optimization/cplex-optimizer/

Le problème à résoudre étant relativement simple mathématiquement, la version
limitée de Baron donne en général satisfaction. Bonmin et Couenne donne des
solutions mais sont (beaucoup) plus lents.



## Licence (voir fichier LICENSE)

The MIT License (MIT) - Copyright (c) 2016 by Mathieu Servillat
