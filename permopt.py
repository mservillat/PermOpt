#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Optimisation de l'attribution des permanences LCDA
# 28 avril 2016, Mathieu Servillat

# Nécessite python 3.5 et le paquet pyomo:
# Pyomo 4.4.1 (VOTD) (CPython 3.5.3 on Darwin 14.5.0)
#   http://www.pyomo.org/
#   Pyomo is a Python-based, open-source optimization modeling language
#   with a diverse set of optimization capabilities.
# Nécessite aussi :
#   matplotlib: http://matplotlib.org/
#   pandas: http://pandas.pydata.org/
#   pyyaml: http://pyyaml.org/wiki/PyYAML

# Nécessite un optimisateur :
#   https://software.sandia.gov/downloads/pub/pyomo/PyomoInstallGuide.html#Solvers
#   Projets open-source :
#     - GLPK (GNU Linear Programming Kit), http://www.gnu.org/software/glpk/
#     - CBC, https://projects.coin-or.org/Cbc
#     - Ipopt, http://www.coin-or.org/download/binary/Ipopt/
#     - bonmin, https://projects.coin-or.org/Bonmin
#   Projets commerciaux (version gratuite limitée) :
#     - Gurobi Optimizer (licence academique possible), http://www.gurobi.com/
#     - IBM ILOG CPLEX Optimizer (version limitée gratuite), http://www-01.ibm.com/software/integration/optimization/cplex-optimizer/
#     - Baron (version limitée gratuite), http://www.minlp.com/baron-downloads
#     - SCIP,

# Rq: Nécessite un solveur quadratique MINLP (mixed-integer nonlinear problems)...
#     ce qui élimine a priori les projets open-source

# Usage:
# python perm.py <fichier_dat_pyomo>

import sys
import os
import glob
import shutil
import yaml
import re
from pyomo.environ import AbstractModel, Set, Param, Var, Objective, Constraint, Integers, Boolean
from pyomo.opt import SolverFactory
import matplotlib.pyplot as plt
from matplotlib import colors
from matplotlib.backends.backend_pdf import PdfPages
import pandas as pd
import datetime as dt
from babel.dates import format_datetime


# ----------
# Config
# ----------

class PermOpt(object):

    def __init__(self, datfile_prefix, cwd='.'):
        # Config par défaut
        self.hmax_consecutives = 2
        self.aprendre = ['_Aprendre']
        self.exclure = {
            'PermConsecutivesMax': [],
            'PermMatinOuAprem': [],
            'PermSoirPuisMatin': [],
        }
        self.solver = 'baron'  # baron, gurobi, bonmin, couenne, cplex
        self.optimiser = True
        # Config interne
        self.datfile_prefix = datfile_prefix
        # Pas besoin d'optimiser si le sondage est contraint (ref)
        if '_ref_' in datfile_prefix:
            self.optimiser = False
        if cwd == '.':
            cwd = os.getcwd()
        self.dat_path = os.path.join(cwd, 'dat')
        self.pdf_path = os.path.join(cwd, 'Permanences_PDF')
        self.png_path = os.path.join(cwd, 'Permanences_PNG')
        self.bilan_path = os.path.join(cwd, 'Bilans')

        # Solvers testés :
        # cbc, glpk : non quadratiques
        # ipopt : non integer...
        # cplex, bonmin, couenne : trop long
        # scip, minlp, Knitro... : licenses nécessaires et limitées
        # gurobi : ok, très rapide, mais installation compliquée + demande licence académique
        # baron : ok, un peu plus long, mais version limitée suffisante !

        # Init
        reps = [
            self.dat_path,
            os.path.join(self.dat_path, 'Archives'),
            self.pdf_path,
            os.path.join(self.pdf_path, 'Archives'),
            self.png_path,
            os.path.join(self.png_path, 'Archives'),
            os.path.join(self.png_path, 'Historique'),
            self.bilan_path
        ]
        for d in reps:
            if not os.path.isdir(d):
                os.mkdir(d)

        self.opt = SolverFactory(self.solver)  #, solver_io=solver_io)
        self.set_model()

    # ----------
    # Modèle pour optimisation
    # ----------

    def set_model(self):

        # Modèle abstrait (données dans un fichier .dat)
        model = AbstractModel()

        # Global variables
        model.hmax_consecutives = self.hmax_consecutives

        # Structure des données
        # Familles :
        model.familles = Set(doc='Noms des familles')
        model.compteur = Param(model.familles, default=0)
        model.coeff = Param(model.familles)
        model.hmin = Param(model.familles)
        model.hmax = Param(model.familles)
        model.avance = Param(model.familles)
        model.autre = Param(model.familles)
        # Permanences
        model.permanences = Set(doc='Dates des permanences')
        model.hdebut = Param(model.permanences, default=0)
        model.hfin = Param(model.permanences, default=0)
        model.duree = Param(model.permanences, default=0)
        model.simult = Param(model.permanences, default=0)
        model.date_jour = Param(model.permanences)
        #model.simult_ii = Set(doc='index des perms simultanees')
        # Souhaits
        model.souhaits = Param(model.familles, model.permanences, default=0)
        model.fixes = Param(model.familles, model.permanences, default=0, mutable=True)

        # Variable à optimiser : attribution des permanences aux familles
        model.x = Var(model.familles, model.permanences, within=Integers, bounds=(0,1), initialize=0)


        # ----------
        # Objectif : compteurs au plus proche de zéro

        # Minimiser la somme quadratique des compteurs
        # Nécessite un solveur quadratique...
        def Objectif(model):
            nheures = sum([model.duree[perm] for perm in model.permanences])
            nheures += sum([model.autre[nom] for nom in model.familles])
            ncoeff = sum([model.coeff[nom] for nom in model.familles])
            if ncoeff == 0:
                ncoeff = 1
            aprendre = float(nheures) / ncoeff
            somme_quadratique_des_compteurs = 0.
            for nom in model.familles:
                if nom not in self.aprendre:
                    # Initialisation du compteur
                    #compteur_famille = 0.
                    compteur_famille = -50.
                    # Rq : le compteur doit toujours rester très négatif,
                    #      sinon les choix secondaires peuvent devenir
                    #      prioritaires au plus proche de zéro...
                    # Compteur avant attribution
                    compteur_famille += model.compteur[nom]
                    # Retrait des heures à faire
                    compteur_famille -= (aprendre * model.coeff[nom])
                    for perm in model.permanences:
                        # Ajout des heures attribuées
                        compteur_famille += model.x[nom, perm] * model.duree[perm] * model.souhaits[nom, perm]
                    # Ajout des autres heures (réunion bureau, ajustement...)
                    compteur_famille += model.autre[nom]
                    # Somme quadratique des compteurs
                    somme_quadratique_des_compteurs += compteur_famille ** 2
            return somme_quadratique_des_compteurs
        model.Objectif = Objective(rule=Objectif)

        # ----------
        # Contraintes

        # Contrainte : parent disponible pour les permanences attribuées
        # Pas necessaire si variable x gelee à 0 quand parent non disponible
        def Disponible(model, nom, perm):
            return model.x[nom, perm] * 0.5 >= model.x[nom, perm] * model.souhaits[nom, perm]
        #model.Disponible = Constraint(model.familles, model.permanences, rule=Disponible)

        # Contrainte : un seul parent par permanence exactement
        def UnParPerm(model, perm):
            # Sauf si aucun souhait pour cette perm
            if sum([model.souhaits[nom, perm] for nom in model.familles]) == 0:
                return Constraint.Feasible
            # Perm attribuée à une seule famille :
            nfpp = sum([model.x[nom, perm] for nom in model.familles])
            return nfpp == 1
        model.UnParPerm = Constraint(model.permanences, rule=UnParPerm)

        # Contrainte : éviter les perms simultannées
        def PermSimult(model, nom, perm1, perm2):
            # Pas de contrainte si perm1==perm2, ou si perms non marquées
            # comme simultannées du même bloc
            if nom in self.aprendre:
                return Constraint.Feasible
            if perm1 == perm2:
                return Constraint.Feasible
            if model.simult[perm1] == 0:
                return Constraint.Feasible
            if model.simult[perm2] == 0:
                return Constraint.Feasible
            if model.simult[perm1] != model.simult[perm2]:
                return Constraint.Feasible
            # Si perm1 commence avant perm2, elle doit aussi se terminer avant
            if model.hdebut[perm1] <= model.hdebut[perm2]:
                return model.x[nom, perm1] * model.x[nom, perm2] * model.hfin[perm1] <= model.hdebut[perm2]
            # Sinon, perm2 commence avant perm1, et doit aussi se terminer avant
            return model.x[nom, perm1] * model.x[nom, perm2] * model.hfin[perm2] <= model.hdebut[perm1]
        model.PermSimult = Constraint(model.familles, model.permanences, model.permanences, rule=PermSimult)

        # Contrainte : éviter les perms consécutives trop longues
        def PermConsecutivesMax(model, nom, perm1, perm2):
            # Pas de contrainte si perm1==perm2, ou si perms non marquées
            # comme simultannées du même bloc
            if nom in self.aprendre + self.exclure['PermConsecutivesMax']:
                return Constraint.Feasible
            if perm1 == perm2:
                return Constraint.Feasible
            if model.date_jour[perm1] != model.date_jour[perm2]:
                return Constraint.Feasible
            # Si perm2 suit perm1, le nombre d'heures consécutives est limité
            if model.hfin[perm1] == model.hdebut[perm2]:
                nheures = model.x[nom, perm1] * model.x[nom, perm2] * (model.duree[perm1] + model.duree[perm2])
                return nheures <= model.hmax_consecutives
            # Si perm1 suit perm2, le nombre d'heures consécutives est limité
            if model.hfin[perm2] == model.hdebut[perm1]:
                # Nombre d'heures consécutives limité
                nheures = model.x[nom, perm1] * model.x[nom, perm2] * (model.duree[perm1] + model.duree[perm2])
                return nheures <= model.hmax_consecutives
            # Sinon, il y a peut-être un chevauchement
            return Constraint.Feasible
        model.PermConsecutivesMax = Constraint(model.familles, model.permanences, model.permanences, rule=PermConsecutivesMax)

        # Contrainte : pas d'autres perm si deja une perm avant 12h
        def PermMatinOuAprem(model, nom, perm1, perm2):
            # Pas de contrainte si perm1==perm2, ou si perms non marquées
            # comme simultannées du même bloc
            if nom in self.aprendre + self.exclure['PermMatinOuAprem']:
                return Constraint.Feasible
            if perm1 == perm2:
                return Constraint.Feasible
            if model.date_jour[perm1] != model.date_jour[perm2]:
                return Constraint.Feasible
            if model.hdebut[perm1] <= model.hdebut[perm2]:
                return model.hdebut[perm1] >= 12.0 * model.x[nom, perm1] * model.x[nom, perm2]
            return model.hdebut[perm2] >= 12.0 * model.x[nom, perm1] * model.x[nom, perm2]
        model.PermMatinOuAprem = Constraint(model.familles, model.permanences, model.permanences, rule=PermMatinOuAprem)

        # Contrainte : pas de perm avant 12h le lendemain d'une perm après 14h
        def PermSoirPuisMatin(model, nom, perm1, perm2):
            # Pas de contrainte si perm1==perm2, ou si perms non marquées
            # comme simultannées du même bloc
            if nom in self.aprendre + self.exclure['PermSoirPuisMatin']:
                return Constraint.Feasible
            if perm1 == perm2:
                return Constraint.Feasible
            if int(model.date_jour[perm2].replace('-','')) - int(model.date_jour[perm1].replace('-','')) == 1:
                if model.hdebut[perm1] >= 14.0:
                    return model.hdebut[perm2] >= 12.0 * model.x[nom, perm1] * model.x[nom, perm2]
            return Constraint.Feasible
        model.PermSoirPuisMatin = Constraint(model.familles, model.permanences, model.permanences, rule=PermSoirPuisMatin)

        # Contrainte : pas plus ou moins d'un certain nombre d'heures de perm par mois
        def PermMinMax(model, nom):
            if nom in self.aprendre:
                return Constraint.Feasible
            nbheures_souhaits = sum([(model.souhaits[nom, perm] > 0) * model.duree[perm] for perm in model.permanences])
            if nbheures_souhaits < model.hmin[nom]:
                return Constraint.Feasible
            nbheures = sum([model.x[nom, perm] * model.duree[perm] for perm in model.permanences])
            #nbheures += model.autre[nom]  # fait planter le code...
            hmin = model.hmin[nom]
            hmax = model.hmax[nom]
            return hmin <= nbheures <= hmax
        model.PermMinMax = Constraint(model.familles, rule=PermMinMax)

        # Contrainte : s'assurer que le compteur final inclus l'avance définie
        def Avance(model, nom):
            if model.avance[nom] == 0:
                return Constraint.Feasible
            else:
                nheures = sum([model.duree[p] for p in model.permanences])
                nheures += sum([model.autre[n] for n in model.familles])
                ncoeff = sum([model.coeff[n] for n in model.familles])
                aprendre = float(nheures) / ncoeff
                compteur_apres = sum([model.x[nom, p] * model.duree[p] for p in model.permanences])
                compteur_apres -= aprendre * model.coeff[nom]
                compteur_apres += model.autre[nom]
                return compteur_apres >= model.avance[nom]
        model.Avance = Constraint(model.familles, rule=Avance)

        # Contrainte : pas plus de N perms en souhait secondaire
        # Pas nécessaire : l'objectif minimise le nombre de perm secondaires attribuées
        def SouhaitsSecondairesMax(model, nom):
            perms = []
            for perm in model.permanences:
                if model.souhaits[nom, perm] == 0.5:
                    perms.append(model.x[nom, perm])
            nbperm = sum(perms)
            if nbperm == 0:
                return Constraint.Feasible
            return nbperm <= 5
        #model.SouhaitsSecondairesMax = Constraint(model.familles, rule=SouhaitsSecondairesMax)

        # Contrainte : nombre de souhaits secondaires plus petits que premiers souhaits
        # Pas nécessaire : l'objectif minimise le nombre de perm secondaires attribuées
        def SouhaitsSecondaires(model, nom):
            poids = []
            perms = []
            for perm in model.permanences:
                poids.append(model.souhaits[nom, perm] * model.x[nom, perm])
                perms.append(model.x[nom, perm])
            moyenne = sum(poids)
            nbperm = sum(perms)
            return moyenne >= 0.9 * nbperm
        #model.SouhaitsSecondaires = Constraint(model.familles, rule=SouhaitsSecondaires)

        # Contrainte : max une permanence non souhaitee par famille
        # Rq : ne pas attribuer les permanences non souhaitees
        def NonSouhaiteeMax(model, nom):
            perms = []
            for perm in model.permanences:
                if model.souhaits[nom, perm] == 0.1:
                    perms.append(model.x[nom, perm])
            nbperm = sum(perms)
            if nbperm == 0:
                return Constraint.Feasible
            return nbperm <= 0
        #model.NonSouhaiteeMax = Constraint(model.familles, rule=NonSouhaiteeMax)

        self.model = model

    # ----------
    # Exécution
    # ----------

    def preparation(self):

        print('\n-----------')
        print('Préparation')
        print('-----------')

        # Input
        datfile_compteurs = self.datfile_prefix + '_compteurs.dat'
        datfile_souhaits = self.datfile_prefix + '_souhaits.dat'
        datfile_config = self.datfile_prefix + '_config.dat'
        # Output
        datfile = self.datfile_prefix + '.dat'

        if os.path.isfile(os.path.join(self.dat_path, datfile_compteurs)):
            print('Fichier des compteurs : {}'.format(datfile_compteurs))
        else:
            print('{} n\'existe pas...'.format(datfile_compteurs))
            sys.exit()

        if os.path.isfile(os.path.join(self.dat_path, datfile_souhaits)):
            print('Fichier des souhaits  : {}'.format(datfile_souhaits))
        else:
            print('{} n\'existe pas...'.format(datfile_souhaits))
            sys.exit()

        # Chargement des modifications
        config = {}
        if os.path.isfile(os.path.join(self.dat_path, datfile_config)):
            print('Fichier de config     : {}'.format(datfile_config))
            with open(os.path.join(self.dat_path, datfile_config), 'r') as fconfig:
                #config = json.load(data_file)['config']
                config = yaml.load(fconfig)
        else:
            print('Création du fichier de config ({})'.format(datfile_config))
            lines = [
                '# ----------',
                'hmax_consecutives: {}'.format(self.hmax_consecutives),
                'solver: {}'.format(self.solver),
                '# ----------',
                'exclure_des_contraintes:',
                '# contrainte = PermConsecutivesMax, PermMatinOuAprem, PermSoirPuisMatin',
                '#  - {nom: , contrainte: }',
                '# ----------',
                'str_replace:',
                '#  - {old: "", new: ""}',
                '# ----------',
                'souhaits_fixe:',
                '# date = 2016-12-31T08h00-09h00',
                '#  - {nom: , date: , fixe: 1/-1}',
                '# ----------',
            ]
            with open(os.path.join(self.dat_path, datfile_config), 'w') as fconfig:
                fconfig.write('\n'.join(lines))
        print('Création du fichier {}'.format(datfile))

        # Set configuration
        if config.get('hmax_consecutives'):
            self.hmax_consecutives = config['hmax_consecutives']
        if config.get('solver'):
            self.solver = config['solver']
        if config.get('exclure_des_contraintes'):
            for ec in config['exclure_des_contraintes']:
                self.exclure[ec['contrainte']].append(ec['nom'])

        # Concaténation des fichiers d'entrée
        with open(os.path.join(self.dat_path, datfile), 'w') as ffinal:
            shutil.copyfileobj(open(os.path.join(self.dat_path, datfile_compteurs), 'r'), ffinal)
            with open(os.path.join(self.dat_path, datfile_souhaits), 'r') as fsouhaits:
                content = fsouhaits.read()
                # Noms à modifier ? {"old": "", "new": ""}
                if config.get('str_replace'):
                    print('-----------')
                    print('Remplacements :')
                    for m in config['str_replace']:
                        content = content.replace(m['old'], m['new'])
                        print('{} --> {}'.format(m['old'], m['new']))
                ffinal.write(content)

        # Instance du modèle avec fichier dat
        instance = self.model.create_instance(os.path.join(self.dat_path, datfile))

        # Souhaits à fixer à la main ? {"nom": "", "date": "", "fixe": 1/-1}
        if config.get('souhaits_fixe'):
            print('-----------')
            print('Permanences fixées :')
            for m in config['souhaits_fixe']:
                instance.fixes[m['nom'], m['date']].value = m['fixe']
                print('{:<10} {:<22} {}'.format(m['nom'], m['date'], m['fixe']))

        # Fixe certaines perm dans la variable à ajuster
        for (i,j) in instance.x:
            # Fixe les perms non souhaitées
            if instance.souhaits[i,j] == 0:
                instance.x[i,j].fixed = True  # moins de variables a optimiser
            if (i not in self.aprendre) and (instance.souhaits[i,j] == 1):
                instance.x[i,j].value = 1  # initialise
            # Perms fixées a la main
            if instance.fixes[i,j].value == 1:
                instance.x[i,j].value = 1
                instance.x[i,j].fixed = True
            elif instance.fixes[i,j].value == -1:
                instance.x[i,j].value = 0
                instance.x[i,j].fixed = True

        # Totaux
        instance.nfamilles = len(instance.familles) - 1
        if instance.nfamilles == 0:
            instance.nfamilles = 1
        instance.ncoeff = round(float(sum([instance.coeff[nom] for nom in instance.familles])), 2)
        if instance.ncoeff == 0:
            instance.ncoeff = 1
        instance.nperm = len(instance.permanences)
        instance.nheures_perm = sum([instance.duree[perm] for perm in instance.permanences])
        instance.nheures_autre = sum([instance.autre[nom] for nom in instance.familles])
        instance.nheures = instance.nheures_perm + instance.nheures_autre
        instance.aprendre = float(instance.nheures) / instance.ncoeff
        instance.datfile = datfile

        self.instance = instance


    def optimisation(self):

        print('-----------')
        print('Optimisation')
        print('-----------')

        # Optimisation
        instance = self.instance
        if self.optimiser:
            self.instance.results = self.opt.solve(instance, tee=True)  #, keepfiles=True)
        else:
            print('\nPas d\'optimisation demandé')

        # Souhaits
        souhaits_dict = {}
        for perm in instance.permanences:
            souhaits_dict[perm] = {}
        # Permanences attribuées par l'optimisation
        perm_attrib_list = []
        for nom, perm in instance.souhaits:
            if nom not in self.aprendre:
                souhaits_dict[perm][nom] = instance.souhaits[nom, perm]
                if instance.x[nom, perm].value:
                    perm_attrib_list.append({
                        'nom': nom,
                        'date': perm,
                        'simult': instance.simult[perm],
                        'hdebut': instance.hdebut[perm],
                        'hfin': instance.hfin[perm],
                        'duree': instance.duree[perm],
                        'souhait': instance.souhaits[nom, perm],
                        'fixe': instance.fixes[nom, perm].value,
                    })
        souhaits_df = pd.DataFrame.from_dict(souhaits_dict, orient='columns')
        souhaits_df = souhaits_df.sort_index()
        souhaits_df = souhaits_df.sort_index(axis=1)
        perm_attrib_cols = ['nom', 'date', 'simult', 'hdebut', 'hfin', 'duree', 'souhait', 'fixe']
        perm_attrib_df = pd.DataFrame(perm_attrib_list)
        if len(perm_attrib_df) > 0:
            perm_attrib_df = perm_attrib_df.sort_values(by=['nom', 'date'])
        # Permanences non attribuées
        perm_non_attrib = []
        for perm in instance.permanences:
            if instance.x[self.aprendre[0], perm].value:
                perm_non_attrib.append(perm)
            elif sum([instance.x[nom , perm].value for nom in instance.familles]) == 0:
                perm_non_attrib.append(perm)
        # Compteurs
        compteurs_dict = {}
        for nom in instance.familles:
            if nom not in self.aprendre:
                compteur_nom = instance.compteur[nom] - (instance.aprendre * instance.coeff[nom])
                nhfait_nom = sum([instance.x[nom, perm].value * instance.duree[perm] for perm in instance.permanences])
                nhfait_nom += instance.autre[nom]
                compteur_nom += nhfait_nom
                compteurs_dict[nom] = {
                    'avant': instance.compteur[nom],
                    'apres': round(compteur_nom, 1),
                    'heures': round(nhfait_nom, 1),
                    'dont_autre': instance.autre[nom]
                }
        compteurs_df = pd.DataFrame.from_dict(compteurs_dict, orient='index')

        self.souhaits_df = souhaits_df
        self.perm_attrib_df = perm_attrib_df
        self.perm_non_attrib = perm_non_attrib
        self.compteurs_df = compteurs_df

        # Ajustement de la somme des compteurs (pour éviter de s'éloigner de 0)
        print('\n')
        self.stat_compteurs()
        if len(self.perm_non_attrib) == 0:
            if self.ecart_apres > 0.1:
                compteurs_df['apres'] -= 0.1
                new_somme_apres = round(sum(compteurs_df['apres']),1)
                print('\nCompteurs ajustés : somme = {} --> {}'.format(self.somme_apres, new_somme_apres))
                self.compteurs_df = compteurs_df
                self.stat_compteurs()
            elif self.ecart_apres < -0.1:
                compteurs_df['apres'] += 0.1
                new_somme_apres = round(sum(compteurs_df['apres']),1)
                print('\nCompteurs ajustés : somme = {} --> {}'.format(self.somme_apres, new_somme_apres))
                self.compteurs_df = compteurs_df
                self.stat_compteurs()
            else:
                print('Pas d\'ajustement des compteurs nécessaire')
        # Affichage de la config
        for c in self.exclure:
            if self.exclure[c]:
                print('! {} sont exclus de la contrainte {}'.format(self.exclure[c], c))
        print('')


    def stat_compteurs(self):
        compteurs_df = self.compteurs_df
        instance = self.instance
        nfam = instance.nfamilles
        if nfam < 1:
            nfam = 1
        self.somme_avant = round(sum(compteurs_df['avant']),1)
        self.ecart_avant = self.somme_avant / nfam
        self.squad_avant = round(sum((compteurs_df['avant'] - self.ecart_avant)**2), 2)
        self.somme_apres = round(sum(compteurs_df['apres']),1)
        self.ecart_apres = self.somme_apres / nfam
        self.squad_apres = round(sum((compteurs_df['apres'] - self.ecart_apres)**2), 2)


    def affichage(self):

        print('\n-----------')
        print('Affichage (PDF et PNG)')

        instance = self.instance
        souhaits_df = self.souhaits_df
        perm_attrib_df = self.perm_attrib_df
        perm_non_attrib = self.perm_non_attrib
        compteurs_df = self.compteurs_df

        # Preparation de la figure
        fig, ax = plt.subplots()
        scale = 2.9
        cmap = colors.ListedColormap(['lightgray', 'orange', 'green'])
        norm = colors.BoundaryNorm([0, 0.5, 1, 2], cmap.N)

        # Affichage des souhaits
        im = ax.imshow(souhaits_df, interpolation='nearest', cmap=cmap, norm=norm)
        im.axes.set_ylim(-0.5, len(souhaits_df.index) - 0.5)
        im.axes.invert_yaxis()
        for k, spine in ax.spines.items():
            spine.set_zorder(10)

        # Lignes entre les jours et préparation des labels des permanences
        ylab = souhaits_df.index.tolist()
        xlab = souhaits_df.columns.tolist()
        xlab_alt = []
        lab_ref = ''
        ax.axvline(-0.5, color='k', lw=1.5, zorder=5)
        ax.axvline(instance.nperm - 0.5, color='k', lw=1.5, zorder=5)
        jour_ref = 'lun'
        for lab in xlab:
            date, times = lab.split('T')
            xlab_alt.append(times)
            if lab.split('T')[0] != lab_ref.split('T')[0]:
                xl = xlab.index(lab) - 0.5
                date_dt = dt.datetime.strptime(date, '%Y-%m-%d')
                jour = format_datetime(date_dt, 'E', locale='fr')
                lw = 1.5
                if 'lun' in jour:
                    lw = 4
                if 'mar' in jour and not 'lun' in jour_ref:
                    lw = 4
                ax.axvline(xl , color='k', lw=lw, zorder=5)
                an2 = ax.annotate('', xy=(xl, -0.5), xycoords='data',
                                  xytext=(xl + 3.35, -6.2), textcoords='data',
                                  arrowprops=dict(arrowstyle='-', lw=lw))
            jour_ref = jour
            lab_ref = lab

        # Affichage d'un point blanc sur les permanences attribuées
        for nom, perm in instance.x:
            if nom not in self.aprendre:
                if instance.x[nom, perm].value:
                    xt = xlab.index(perm)
                    yt = ylab.index(nom)
                    ax.plot(xt, yt, 'o', color='w', ms=8)

        # Permanences non attribuées en rouge
        for perm in perm_non_attrib:
            ax.axvspan(xlab.index(perm) - 0.5, xlab.index(perm) + 0.5, color='r', alpha=.1)

        # Affichage des labels des permanences
        im.axes.xaxis.set_ticks_position('top')
        im.axes.xaxis.set_ticks([x + .5 for x in range(instance.nperm)], minor=True)
        im.axes.xaxis.set_ticks([x for x in range(instance.nperm)])
        im.axes.xaxis.set_ticklabels(xlab_alt, rotation=60, ha='left')

        # Affichage des noms des familles
        im.axes.yaxis.set_ticks([x + .5 for x in range(instance.nfamilles)], minor=True)
        im.axes.yaxis.set_ticks(range(instance.nfamilles))
        im.axes.yaxis.set_ticklabels(list(map(lambda s: re.sub( '(?<!^)(?=[A-Z])', ' ', s), ylab)), fontsize=15)
        ax.tick_params(which='both', bottom='off', top='off', left='off', right='off')

        # Grille blanche (minor ticks)
        im.axes.grid(False)
        im.axes.grid(True, which='minor', color='w',linestyle='-', lw=1.5)

        # Affichage du mois
        date_dt = dt.datetime.strptime(xlab[0].split('T')[0], '%Y-%m-%d')
        mois = format_datetime(date_dt, 'MMMM yyyy', locale='fr')
        ax.text(0.8, -3.6, mois, rotation=60, ha='center', va='center', fontsize=16, fontweight='bold')

        # Affichage par page (PDF pour impression)
        npermparpage = instance.nperm
        if instance.nperm > 28:
            npermparpage = int(round(instance.nperm/2. + 1))
        if instance.nperm > 56:
            npermparpage = int(round(instance.nperm/3. + 1))
        fig_height = (1 + instance.nfamilles + 6.5) / scale
        fig_width  = (4 + npermparpage + 4) / scale
        fig.set_size_inches(fig_width, fig_height)
        fig.subplots_adjust(
            bottom = (1) / (fig_height * scale),
            top    = (1 + instance.nfamilles) / (fig_height * scale),
            left   = (4) / (fig_width * scale),
            right  = (4 + npermparpage) / (fig_width * scale),
        )
        with PdfPages(os.path.join(self.pdf_path, self.datfile_prefix + '.pdf')) as pdf:
            pages = range(0, instance.nperm, npermparpage)
            for p in pages:
                p2 = p + npermparpage
                if p2 > instance.nperm:
                    p2 = instance.nperm
                im.axes.set_xlim(p - 0.5, p2 - 0.5)
                plab = ax.text(.02, .92, u'Permanences parents\n{}\npage {}/{}'.format(mois, int(p/npermparpage+1), len(pages)), fontsize=9, transform=fig.transFigure)
                # mois
                mois_ax = ax.text(p + 0.8, -3.6, mois, rotation=60, ha='center', va='center', fontsize=16, fontweight='bold')
                mois_an = ax.annotate('', xy=(p - 0.5, -0.5), xycoords='data',
                                      xytext=(p - 0.5 + 3.35, -6.2), textcoords='data',
                                      arrowprops=dict(arrowstyle='-', lw=1.5))
                # Noms des jours en gras
                lab_ref = ''
                days_ax = []
                for lab in xlab[p:p2]:
                    date, times = lab.split('T')
                    if lab.split('T')[0] != lab_ref.split('T')[0]:
                        xl = xlab.index(lab) - 0.5
                        date_dt = dt.datetime.strptime(date, '%Y-%m-%d')
                        day = format_datetime(date_dt, 'E', locale='fr')
                        daynum = '{} {}'.format(day, date_dt.day)
                        if date.split('-')[1] != xlab[0].split('T')[0].split('-')[1]:
                            daynum = '{} {}/{}'.format(day, date_dt.day, date_dt.month)
                        days_ax.append(ax.text(xl + 2.28, -3.75, daynum, rotation=60, ha='left', va='bottom', fontweight='bold', fontsize=14))
                    lab_ref = lab
                # Noms des familles le long de chaque ligne
                noms_ax = []
                for nom in instance.familles:
                    if nom not in self.aprendre:
                        nom_aff = re.sub( '(?<!^)(?=[A-Z])', ' ', nom)
                        for xx in range(p + 4, p2 - 1, 6):
                            noms_ax.append(ax.text(xx + 0.5, ylab.index(nom), nom_aff, va='center', ha='center', alpha=0.4, fontsize=12))
                # Enregistrer la page
                pdf.savefig(im.figure)
                # Enlever l'affichage spécifique à cette page
                plab.remove()
                mois_ax.remove()
                mois_an.remove()
                for o in noms_ax:
                    o.remove()
                for o in days_ax:
                    o.remove()
            d = pdf.infodict()
            d['Title'] = 'Permanences de ' + mois
            d['Author'] = u'VP LCDA'
            d['CreationDate'] = dt.datetime.today()

        # Tout afficher (png pour contrôle)
        fig_height = (1 + instance.nfamilles + 6.5) / scale
        fig_width  = (4 + instance.nperm + 7) / scale
        fig.set_size_inches(fig_width, fig_height)
        fig.subplots_adjust(
            bottom = (1) / (fig_height * scale),
            top    = (1 + instance.nfamilles) / (fig_height * scale),
            left   = (4) / (fig_width * scale),
            right  = (4 + instance.nperm) / (fig_width * scale),
        )
        im.axes.set_xlim(-0.5, len(souhaits_df.columns) - 0.5)
        # Noms des jours en gras
        lab_ref = ''
        days_ax = []
        for lab in xlab:
            date, times = lab.split('T')
            if lab.split('T')[0] != lab_ref.split('T')[0]:
                xl = xlab.index(lab) - 0.5
                date_dt = dt.datetime.strptime(date, '%Y-%m-%d')
                day = format_datetime(date_dt, 'E', locale='fr')
                daynum = '{} {}'.format(day, date_dt.day)
                if date.split('-')[1] != xlab[0].split('T')[0].split('-')[1]:
                    daynum = '{} {}/{}'.format(day, date_dt.day, date_dt.month)
                days_ax.append(ax.text(xl + 2.28, -3.75, daynum, rotation=60, ha='left', va='bottom', fontweight='bold', fontsize=14))
            lab_ref = lab
        # Noms des familles le long de chaque ligne
        for nom in instance.familles:
            if nom not in self.aprendre:
                nom_aff = re.sub( '(?<!^)(?=[A-Z])', ' ', nom)
                for xx in range(4, instance.nperm - 1, 6):
                    ax.text(xx + 0.5, ylab.index(nom), nom_aff, va='center', ha='center', alpha=0.4, fontsize=12)
        # Bilan des compteurs
        htot = '({}+{}) h / {} = {} h'.format(instance.nheures_perm, instance.nheures_autre, instance.ncoeff, round(instance.aprendre, 2))
        compteurs_ax = []
        compteurs_ax.append(ax.text(instance.nperm + 0.05, -1.15, 'heures a prendre :', va='center', fontsize=9, alpha=0.5))
        compteurs_ax.append(ax.text(instance.nperm + 0.05, -0.75, htot, va='center', fontsize=9, alpha=0.5))
        for nom in instance.familles:
            if nom not in self.aprendre:
                cpt = compteurs_df.loc[nom]
                compteur_nom_str = r'{:>5} $\rightarrow${:>5} ({})'.format(str(cpt['avant']), str(cpt['apres']), str(cpt['heures']))
                compteurs_ax.append(ax.text(instance.nperm - 0.2, ylab.index(nom), compteur_nom_str, va='center', fontname='monospace', fontsize=12))
        if instance.nfamilles > 1:
            #somme_apres = round(sum(compteurs_df['apres']),1)
            #ecart_apres = somme_apres / instance.nfamilles
            #squad_apres = round(sum((compteurs_df['apres'] - ecart_apres)**2), 2)
            squad = r'$\Sigma^2$ = {}'.format(self.squad_apres)
            compteurs_ax.append(ax.text(instance.nperm + 3.9, instance.nfamilles - 0.15, squad, va='center', ha='right', fontsize=12, alpha=0.5))

        # Enregistrer la figure
        fpng_base = os.path.join(self.png_path, self.datfile_prefix)
        plt.savefig(fpng_base + '.png', format='png')
        # Garder l'historique de la figure
        fpng_hist = os.path.join(self.png_path, 'Historique', self.datfile_prefix)
        ll = glob.glob(fpng_hist + '_v??.png')
        if ll:
            ll.sort()
            ilast = ll[-1].split(fpng_hist + '_v')[-1].split('.png')[0]
            inext = int(ilast) + 1
        else:
            inext = 1
        fpng = fpng_hist + '_v{:02d}.png'.format(inext)
        plt.savefig(fpng, format='png')


    def bilan(self):

        instance = self.instance
        perm_attrib_df = self.perm_attrib_df
        perm_non_attrib = self.perm_non_attrib
        compteurs_df = self.compteurs_df

        # Nombre de perm attribuées (choix principaux et choix secondaires)
        nattrib1 = len(perm_attrib_df)
        if len(perm_attrib_df) > 0:
            nattrib2 = len(perm_attrib_df.loc[perm_attrib_df['souhait'] == 0.5])
        else:
            nattrib2 = 0
        # Nombre de perm non attribuées et heures
        npexcl = len(perm_non_attrib)
        nhexcl = sum([instance.duree[perm] for perm in perm_non_attrib])
        # Mois
        mois = instance.datfile.split('_')[1]
        perms = list(instance.permanences)
        d1 = min(perms).split('T')[0].split('-')
        d2 = max(perms).split('T')[0].split('-')
        date_debut = d1[2] + '/' + d1[1]
        date_fin = d2[2] + '/' + d2[1]

        # compteurs
        compteur_string = compteurs_df[['avant', 'apres', 'heures', 'dont_autre']].to_string()
        ncar = len(compteur_string.split('\n')[0])

        lines = []
        lines.append(ncar * '-')
        lines.append('Bilan ' + mois)
        lines.append(ncar * '-')

        # Affichage du bilan
        #instance.x.display()
        #instance.souhaits.display()
        #instance.duree.display()
        #instance.compteur.display()
        #lines.append('Permanences :')
        #lines.append(perm_attrib_df.to_string(index=False, justify='left'))
        #lines.append('-----------')

        # Perm simultannees
        # if len(perm_attrib_df) > 0:
        #     nsimult = max(perm_attrib_df['simult']) + 1
        # else:
        #     nsimult = 0
        # for i in range(1, nsimult):
        #     perm_simult = perm_attrib_df.loc[perm_attrib_df['simult'] == i]
        #     if len(perm_simult) >= 2:
        #         for nom in instance.familles:
        #             perm_simult_nom = perm_simult.loc[perm_simult['nom'] == nom]
        #             if len(perm_simult_nom) >= 2:
        #                 lines.append('! Permanences simultannees (fixer certaines a -1 dans les souhaits ?) :')
        #                 #lines.append(perm_simult_nom.to_string(columns=perm_attrib_cols, index=False, justify='left')
        #                 for i in perm_simult_nom.index:
        #                     row = perm_simult_nom.loc[i]
        #                     lines.append('  - {{nom: {}, date: {}, fixe: -1}}'.format(row['nom'], row['date'], row['fixe']))
        #                 lines.append('-----------')

        # Bilan

        lines.append('Sondage Doodle         : www.doodle.com/poll/{}'.format(self.datfile_prefix.split('_')[-1]))
        lines.append('Nombre de familles     : {}'.format(instance.nfamilles))
        lines.append('Somme des coefficients : {}'.format(instance.ncoeff))
        lines.append('A prendre par famille  : {} h ({} h / {})'.format(round(instance.aprendre, 2), instance.nheures, instance.ncoeff))
        lines.append('Nombre de permanences  : {}'.format(instance.nperm))
        sinec = ' (dont {} souhait{s} \"si nécessaire\")'.format(nattrib2, s='s' if nattrib2 > 1 else '')
        lines.append('Permanences attribuées : {}{sinec}'.format(nattrib1, sinec=sinec))  # if nattrib2 > 0 else ''))
        lines.append('Reste à prendre       : {:>5} h   ({} permanence{s})'.format(nhexcl, npexcl, s='s' if npexcl > 1 else ''))
        if instance.nfamilles > 1:
            #somme_avant = round(sum(compteurs_df['avant']),1)
            #ecart_avant = somme_avant / instance.nfamilles
            #squad_avant = round(sum((compteurs_df['avant'] - ecart_avant)**2), 2)
            #somme_apres = round(sum(compteurs_df['apres']),1)
            #ecart_apres = somme_apres / instance.nfamilles
            #squad_apres = round(sum((compteurs_df['apres'] - ecart_apres)**2), 2)
            lines.append('Somme compteurs avant : {: 5.1f} h   (somme quadratique : {:6.2f})'.format(self.somme_avant, self.squad_avant))
            lines.append('Somme compteurs après : {: 5.1f} h   (somme quadratique : {:6.2f})'.format(self.somme_apres, self.squad_apres))
            lines.append(ncar * '-')
            lines.append('Compteurs {} (du {} au {}) :'.format(mois, date_debut, date_fin))
            lines.append(compteur_string)
            lines.append(ncar * '-')

        # Affichage et écriture
        for l in lines:
            print(l)
        datfile_bilan = self.datfile_prefix + '_bilan.txt'
        with open(os.path.join(self.bilan_path, datfile_bilan), 'w') as f:
            f.write('\n'.join(lines))

    def execution(self):
        self.preparation()
        self.optimisation()
        self.affichage()
        self.bilan()


# ----------
# Main
# ----------


if __name__ == '__main__':
    datfile_prefix = 'perm_2017-01_d2magctkyqw9uff4'  # .dat par defaut
    if len(sys.argv) > 1:
        datfile_prefix = sys.argv[1]
    datfile_prefix = datfile_prefix.split('/')[-1]
    # Init PermOpt
    cwd = os.path.abspath(os.path.dirname(__file__))  # os.getcwd()
    permopt = PermOpt(datfile_prefix, cwd=cwd)
    permopt.execution()
