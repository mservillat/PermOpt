#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Interface graphique pour PermOpt
# 25 octobre 2016, Mathieu Servillat
# Infos sur tkinter:
# https://www.tutorialspoint.com/python3/python_gui_programming.htm

# Make app
# use AppleScript editor, then save as Application
# tell application "Terminal"
#     do script with command "python /path_to/permopt_gui.py;exit"
# end tell


import io
import os
import sys
import time
import glob
# Utiliser Tkinter pour python 2, tkinter pour python 3
from tkinter import *
#from tkinter import *
from tkinter import messagebox
# Pour afficher des PNG avec tkinter 8.5
from PIL import Image, ImageTk
from souhaits import PermSouhaits
#from permopt import PermOpt
import subprocess

dat_path = 'dat'
png_path = 'Permanences_PNG'
bilan_path = 'bilans'

class IORedirector(object):
   '''A general class for redirecting I/O to this Text widget.'''
   def __init__(self,text_area):
       self.text_area = text_area
       self.fileno = sys.stdout.fileno
   def flush(self):
       pass

class StdoutRedirector(IORedirector):
   '''A class for redirecting stdout to this Text widget.'''
   def write(self, message):
       self.text_area.config(state = "normal")
       self.text_area.see(END)
       self.text_area.insert("end", message)
       self.text_area.config(state = "disabled")
       self.text_area.see(END)

class PopupWindow(object):
    def __init__(self, master, text=''):
        top = self.top = Toplevel(master)
        self.l = Label(top, text="Configuration")
        self.l.pack()
        self.t = Text(top, relief=GROOVE, bd=2, highlightthickness=0)
        self.t.config(width=80, height=20)
        self.t.pack()
        self.b = Button(top, text='Ok', command=self.cleanup)
        self.b.pack()
        self.value = text
        self.t.delete(1.0, END)
        self.t.insert(END, text)
    def cleanup(self):
        config_str = self.t.get(1.0, END)
        if config_str and config_str[-1] == '\n':
            config_str = config_str[0:-1]
        self.value = config_str
        self.top.destroy()


class PermOpt_GUI(Tk):

    def __init__(self, parent, path):
        Tk.__init__(self, parent)
        self.title('Attribution des Permanences')
        self.parent = parent
        self.img_width_max = 1200.
        self.img_height_max = 350  # 340.
        self.text_width = 120
        self.cpt_width = 55
        self.cpt_height = 21
        self.w = 0
        self.h = 200
        self.path = path
        self.poll_list = []
        self.initialize()

    def initialize(self):
        self.grid()
        # Poll Entry
        self.poll_label = Label(self, text="Sondage : ", anchor=W)
        self.poll_value = StringVar()
        self.poll = Entry(self, textvariable=self.poll_value)
        # Liste des poll_id à partir des fichiers présents
        self.mb_value = StringVar()
        self.mb_value.set("Sondages disponibles")
        self.mb = Menubutton(self, textvariable=self.mb_value)
        self.mb.menu = Menu(self.mb, tearoff=0)
        self.mb["menu"] = self.mb.menu
        self.reload_dat_list()
        #for p in self.poll_list:
        #    self.mb.menu.add_checkbutton(label=p, command=lambda p=p: self.afficher_compteurs_et_tableau(p))
        #self.mb.bind("<Return>", self.OnPressEnter)
        # Boutons
        self.actions_label = Label(self, text="Actions : ", anchor=W)
        self.b1 = Button(self, text="Doodle", command=self.charger_souhaits)
        self.b2 = Button(self, text="MaJ Compteurs", command=self.maj_compteurs)
        self.b3 = Button(self, text="Sauver", command=self.sauver_compteurs)
        self.b4 = Button(self, text="Config", command=self.popup_config)
        self.b5 = Button(self, text="PermOpt", command=self.lancer_permopt)
        self.b6 = Button(self, text="Bilan", command=self.afficher_bilan)
        # Compteurs
        self.compteurs = Text(self, relief=GROOVE, bd=2, highlightthickness=0)
        self.compteurs.config(width=self.cpt_width, height=self.cpt_height)
        # Stdout
        self.stdout_label = Label(self, text="Messages d'exécution", anchor=W)
        self.stdout = Text(self, wrap = 'word', relief=GROOVE, bd=2)
        self.stdout.bind("<1>", lambda event: self.stdout.focus_set())
        #self.stdout.bind("<Key>", lambda e: "break")
        sys.stdout = StdoutRedirector(self.stdout)
        # Tableau des Permanences
        self.frame = Frame(self)
        self.canvas = Canvas(self.frame, bg="white")
        #self.hbar = Scrollbar(self.frame, orient=HORIZONTAL)
        #self.hbar.pack(side=BOTTOM, fill=X)
        self.canvas.pack(expand=True, fill=BOTH)
        # config
        self.canvas.config(width=self.w, height=self.h)
        self.frame.config(width=self.w, height=self.h)
        #self.canvas.config(xscrollcommand=self.hbar.set)
        #self.hbar.config(command=self.canvas.xview)
        #self.canvas.config(scrollregion=(0,0,self.w,self.h))
        self.compteurs.config(width=self.cpt_width)
        self.stdout.config(width=self.text_width)
        # Grid
        self.poll_label.grid(row=0, column=0)
        self.poll.grid(row=0, column=1, columnspan=5, sticky='EW')
        self.mb.grid(row=1, column=1, columnspan=5, sticky='EW')
        #self.actions_label.grid(row=2, column=0)
        self.b1.grid(row=2, column=0, sticky='EW')
        self.b2.grid(row=2, column=1, sticky='EW')
        self.b3.grid(row=2, column=2, sticky='EW')
        self.b4.grid(row=2, column=3, sticky='EW')
        self.b5.grid(row=2, column=4, sticky='EW')
        self.b6.grid(row=2, column=5, sticky='EW')
        self.compteurs.grid(row=3, column=0, columnspan=6, sticky='EW')
        self.stdout_label.grid(row=0, column=6)
        self.stdout.grid(row=1, column=6, rowspan=3, sticky = 'NSEW')
        self.frame.grid(row=4, column=0, columnspan=7, sticky='EW')
        # Affichage dernier sondage
        if len(self.poll_list) and self.poll_list[-1]:
            self.afficher_compteurs_et_tableau(self.poll_list[-1])
            self.update()
        #self.geometry(self.geometry())
        self.poll.focus_set()
        self.lift()
        self.attributes('-topmost', True)
        self.attributes('-topmost', False)

    def logger(self, message):
        self.stdout.config(state = "normal")
        self.stdout.see(END)
        self.stdout.insert("end", "--> " + message)
        if message and message[-1] != '\n':
            self.stdout.insert("end", '\n')
        self.stdout.config(state = "disabled")
        self.stdout.see(END)
        self.update()

    def popup_config(self):
        p = self.poll_value.get()
        self.logger('Edition de la configuration pour ' + p)
        fn = os.path.join(self.path, dat_path, p + '_config.dat')
        with open(fn, 'r') as fp:
            text = fp.read()
        self.b5.config(state=DISABLED)
        self.pw = PopupWindow(self, text)
        self.pw.t.focus_set()
        self.wait_window(self.pw.top)
        # Executé seulement quand la fenêtre est détruite :
        self.b5.config(state=NORMAL)
        if self.pw.value != '':
            with open(fn, 'w') as fp:
                fp.write(self.pw.value)
            self.logger('Configuration sauvegardée pour {} :\n{}'.format(p, self.pw.value))

    def reload_dat_list(self):
        self.dat_list = glob.glob(os.path.join(self.path, dat_path, '*_compteurs.dat'))
        #self.dat_list.sort(reverse=True)
        self.poll_list = []
        for dat in self.dat_list:
            p = '_'.join(os.path.basename(dat).split('_')[0:-1])
            self.poll_list.append(p)
            try:
                i = self.mb.menu.index(p)
                self.mb.menu.entryconfigure(i, label=p, command=lambda p=p: self.afficher_compteurs_et_tableau(p))
            except:
                self.mb.menu.insert_checkbutton(0, label=p, command=lambda p=p: self.afficher_compteurs_et_tableau(p))

    def afficher_tableau(self):
        p = self.poll_value.get()
        self.logger('Affichage du tableau des permanences ({})'.format(p))
        fn = os.path.join(self.path, png_path, p + '.png')
        if os.path.isfile(fn):
            self.logger('Tableau généré le {}'.format(time.ctime(os.path.getmtime(fn))))
            self.img = Image.open(fn)
            [w, h] = self.img.size
            self.img_scale = self.img_width_max / w
            if self.img_scale * h > self.img_height_max:
                self.img_scale = self.img_height_max / h
            self.h = int(h * self.img_scale)
            self.w = int(w * self.img_scale)
            self.img = self.img.resize((self.w,self.h), Image.ANTIALIAS)
            self.imgtk = ImageTk.PhotoImage(self.img)
            self.frame.config(width=self.h, height=self.h)
            self.canvas.config(width=self.w, height=self.h)
            self.canvas.config(scrollregion=(0,0,self.h,self.h))
            self.canvas.create_image(self.w/2, self.h/2, image=self.imgtk)
            self.update()
        else:
            self.logger('Fichier png non trouvé : ' + fn)

    def afficher_bilan(self):
        p = self.poll_value.get()
        self.logger('Affichage du dernier bilan pour ' + p)
        # Affiche dernier bilan
        fn = os.path.join(self.path, bilan_path, p + '_bilan.txt')
        if os.path.isfile(fn):
            self.logger('Bilan généré le {}'.format(time.ctime(os.path.getmtime(fn))))
            with open(fn, 'r') as fin:
                print(fin.read())
        else:
            self.logger('Pas encore de bilan : lancer PermOpt')


    def afficher_compteurs(self, p):
        self.logger('Affichage des compteurs pour ' + p)
        self.poll_value.set(p)
        fn = os.path.join(self.path, dat_path, p + '_compteurs.dat')
        with open(fn, 'r') as fp:
            self.compteurs.delete(1.0, END)
            self.compteurs.insert(END, fp.read())

    def afficher_compteurs_et_tableau(self, p):
        self.afficher_compteurs(p)
        self.afficher_tableau()

    def charger_souhaits(self):
        poll_id = self.poll_value.get().split('_')[-1]
        self.logger('Connection au Doodle ' + poll_id)
        permsouhaits = PermSouhaits(poll_id, self.path)
        datfile_prefix = permsouhaits.charger_souhaits()
        self.poll_value.set(datfile_prefix)
        self.afficher_compteurs(datfile_prefix)
        self.reload_dat_list()

    def maj_compteurs(self):
        p = self.poll_value.get()
        datfile_compteurs = p + '_compteurs.dat'
        poll_id = p.split('_')[-1]
        permsouhaits = PermSouhaits(poll_id, self.path)
        familles_df = permsouhaits.lire_compteurs(datfile_compteurs)
        familles_avant = permsouhaits.compteurs_avant(datfile_compteurs)
        if familles_avant:
            for nom in familles_avant:
                if nom in familles_df.index:
                    familles_df.set_value(nom, 'compteur', familles_avant[nom]['compteur'])
                    print('Mise à jour du compteur pour {} : {}'.format(nom, familles_avant[nom]['compteur']))
        permsouhaits.ecrire_compteurs(familles_df, datfile_compteurs)
        self.afficher_compteurs(p)

    def sauver_compteurs(self):
        p = self.poll_value.get()
        self.logger('Sauvegarde du fichier compteur pour ' + p)
        compteurs_str = self.compteurs.get(1.0, END)
        if compteurs_str and compteurs_str[-1] == '\n':
            compteurs_str = compteurs_str[0:-1]
        if compteurs_str != '':
            fn = os.path.join(self.path, dat_path, p + '_compteurs.dat')
            with open(fn, 'w') as fp:
                fp.write(compteurs_str)
            #msg = messagebox.showinfo("PermOpt", "Fichier enregistré:\n" + fn)

    def lancer_permopt(self):
        p = self.poll_value.get()
        self.sauver_compteurs()
        self.logger('Optimisation en cours... (' + p + ')')
        self.update()
        try:
            #result = subprocess.run('python permopt.py' + p, stdout=subprocess.PIPE)
            #self.logger(result.stdout.decode())
            filename = self.path + '/temp.log'
            with io.open(filename, 'w') as writer, io.open(filename, 'r') as reader:
                process = subprocess.Popen(['python', self.path + '/permopt.py', p], stdout=writer)
                while process.poll() is None:
                    writer.flush()
                    message = reader.read()
                    self.logger(message)
                    time.sleep(1)
                # Read the remaining
                message = reader.read()
                self.logger(message)
            self.logger('\n')
            self.logger('Optimisation terminée (' + p + ')')
            self.afficher_tableau()
        except Exception as e:
            self.logger('Erreur pendant l\'optimisation (' + p + ')\n' + str(e))

if __name__ == "__main__":
    cwd = os.path.abspath(os.path.dirname(__file__))  # os.getcwd()
    app = PermOpt_GUI(None, cwd)
    #app.protocol("WM_DELETE_WINDOW", app.quit)
    app.mainloop()
