#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Optimisation de l'attribution des permanences LCDA - Lecture des souhaits
# 28 avril 2016, Mathieu Servillat

# Nécessite python et les paquets :
#   requests: http://docs.python-requests.org/en/master/
#   pandas: http://pandas.pydata.org/

# Usage:
# python souhaits.py <poll_id>

import sys, os, glob
import requests
import json
from io import StringIO
import unicodedata
import datetime as dt
import pytz
from tzlocal import get_localzone # $ pip install tzlocal
from babel.dates import format_datetime
import pandas as pd


# ----------
# Config
# ----------

class PermSouhaits(object):

    def __init__(self, poll_id, cwd='.'):
        self.poll_id = poll_id
        self.hmax_mois = 20  # Nombre max d'heures par mois
        self.simu_active = False  # Activer la simulation des souhaits
        self.simu_garder = False  # Garder les souhaits du doodle dans la simu
        self.simu_nsouhaits = [20, 0]  # Souhaits et souhaits secondaires dans la simu
        if cwd == '.':
            cwd = cwd = os.path.abspath(os.path.dirname(__file__))  # os.getcwd()
        self.path = cwd
        self.dat_path = cwd + '/dat'
        self.decalage = 0  # décalage des heures (pb tz ?), à partir du fichier <self.dat_path>/decalage_<poll_id>.dat
        # Init
        if not os.path.isdir(self.dat_path):
            os.mkdir(self.dat_path)


# ----------
# Fonctions
# ----------


    def clean_name(self, familles_list):
        """Enleve accents et espaces aux noms des enfants"""
        # Clean familles
        familles = []
        for f in familles_list:
            # remove after "," or " "
            fname = f.split(',')[0].title().replace(' ','').replace('&','')
            # to ascii
            fname = unicodedata.normalize('NFKD', fname).encode('ascii', 'ignore')
            familles.append(fname.decode())
        return familles

    def load_doodle(self):
        poll_id = self.poll_id
        # Get doodle HTML page
        # New API returns JSON !
        url = 'https://doodle.com/api/v2.0/polls/' + poll_id
        print(url)
        try:
            resp = requests.get(url)  # , cookies={'timeZone': 'Europe/Paris'})
        except requests.exceptions.ConnectionError as e:
            print('Erreur de connection')
            raise
        print(resp)
        if resp.status_code != 200:
            print('URL innaccessible')
            raise
        # Convert to dict and lists
        poll_dict = json.load(StringIO(resp.text))
        #print(resp.text)
        #print(json.dumps({k:v for k,v in poll_dict.items() if k not in ['participants','options']}, indent=4, sort_keys=True))
        permanences_list = poll_dict['options']
        familles_list = poll_dict['participants']

        # Affichage titre et options du sondage
        print('Sondage : {}'.format(poll_dict.get('title', 'Sans titre')))
        # yesno poll?
        print('Type de sondage : {}'.format(poll_dict.get('preferencesType', 'Sans type')))
        yesno = False
        if poll_dict.get('preferencesType') == 'YESNO':
            yesno = True
        # Is constrained?
        contraintes = False
        if poll_dict.get('columnConstraint', 0) == 1:
            contraintes = True
            print('Contrainte : 1 seul parent par permanence')
        # Date dernier changement
        if 'latestChange' in poll_dict:
            latestChange = dt.datetime.fromtimestamp(poll_dict['latestChange'] / 1e3)
            print('Dernière activité : {}'.format(latestChange))
        # Fuseau horaire
        creche_tz = pytz.timezone('Europe/Paris')
        local_tz = get_localzone()
        # Décalage si le sondage a été créé avec la version précédente de doodle (?)
        # sans doute lié aux timezones...
        # Indiquer alors le décalage dans un fichier :
        fdecalage = os.path.join(self.dat_path, 'decalage_' + self.poll_id + '.dat')
        if os.path.isfile(fdecalage):
            with open(fdecalage, 'r') as f:
                self.decalage = int(f.read())
            local_tz = pytz.utc
            creche_tz = pytz.utc
            print('Décalage : ' + str(self.decalage))
        print('Fuseau horaire : {} --> {}'.format(local_tz, creche_tz))

        # Liste des familles
        familles_noms = [f['name'] for f in familles_list]
        familles_souhaits = [f['preferences'] for f in familles_list]
        #print(familles_souhaits)
        # Clean familles
        familles_noms = self.clean_name(familles_noms)
        print('Familles : {}'.format(', '.join(familles_noms)))

        # Liste des permanences
        permanences = []
        for p in permanences_list:
            #ps = p.split(u' \u2013 ')
            #date_str = ps[0]
            #date_dt = dt.datetime.strptime(date_str, '%a %m/%d/%y %I:%M %p')
            #hfin_str = ps[1]
            #hfin_dt = dt.datetime.strptime(hfin_str, '%I:%M %p')
            #date_dt_fin = dt.datetime.combine(date_dt.date(), hfin_dt.time())
            date_dt = dt.datetime.fromtimestamp((p['start'] / 1e3) + (self.decalage * 3600))
            date_dt_fin = dt.datetime.fromtimestamp((p['end'] / 1e3) + (self.decalage * 3600))
            date_dt = local_tz.localize(date_dt)
            date_dt_fin = local_tz.localize(date_dt_fin)
            #print(str(date_dt) + ' - ' + str(date_dt_fin))
            date_dt = date_dt.astimezone(creche_tz)
            date_dt_fin = date_dt_fin.astimezone(creche_tz)
            #print(' --> ' + str(date_dt) + ' - ' + str(date_dt_fin))
            date_jour = date_dt.strftime('%Y-%m-%d')
            hdebut_str = date_dt.strftime('%Hh%M')
            hfin_str = date_dt_fin.strftime('%Hh%M')
            date = '{}T{}-{}'.format(date_jour, hdebut_str, hfin_str)
            hdebut = float(date_dt.strftime('%H.%M'))
            hfin = float(date_dt_fin.strftime('%H.%M'))
            # Si hfin = '##h01' ou '##h02', alors hfin = '##h00'
            if hfin - int(hfin) <= 0.02:
                hfin = round(hfin)
            duree_dt = date_dt_fin - date_dt
            duree = round(duree_dt.seconds / 3600., 1)
            jour = format_datetime(date_dt, 'EEEE', locale='fr')
            permanences.append({
                'date': date,            # nom unique de la permanence
                'duree': duree,          # duree en heures
                'date_jour': date_jour,  # '%Y-%m-%d'
                'hdebut': hdebut,         # float, 8.0, 8.30, ...
                'hfin': hfin,             # float, 9.0, 9.30, ...
                'jour': jour,            # nom du jour en francais
            })

        # Souhaits des familles
        souhaits = []
        for i, nom in enumerate(familles_noms):
            for j, s in enumerate(familles_souhaits[i]):
                if s:
                    s_val = s
                    if not yesno:
                        s_val = s_val/2
                    souhaits.append({
                        'nom': nom,
                        'date': permanences[j]['date'],
                        'souhait': s_val,
                        'fixe': 0,
                    })

        self.familles_noms = familles_noms
        self.permanences = permanences
        self.souhaits = souhaits
        self.contraintes = contraintes


    def load_doodle_old(self):
        poll_id = self.poll_id
        # Get doodle HTML page
        url = 'http://doodle.com/poll/' + poll_id
        print(url)
        try:
            # Attention : problèmes avec Doodle Beta version
            # - impossible de localiser les infos du sondage
            # - possibilité d'export XLS, mais manuel
            # - cookies={'d-betaCode': 'true'} permet d'utiliser l'ancienne version de doodle
            resp = requests.get(url, cookies={'d-betaCode': 'true'})
        except requests.exceptions.ConnectionError as e:
            print('Erreur de connection')
            raise
        print(resp)
        if resp.status_code != 200:
            print('URL innaccessible')
            raise
        # Find poll data
        lines = resp.text.split('\n')
        poll_line = ''
        with open(os.path.join(self.dat_path, 'doodle_{}.html'.format(self.poll_id)), 'w') as fout:
            for line in lines:
                fout.write(line)
                if '$.extend(true, doodleJS.data, {"poll"' in line:
                    print('Sondage détecté !')
                    poll_line = line
        if not poll_line:
            raise Warning('Pas de sondage dans la page Doodle (mise à jour de Doodle en version beta ?)')
        # Extract json
        poll_json = poll_line.split('$.extend(true, doodleJS.data, ')[-1][:-2]
        # Convert to dict and lists
        poll_dict = json.load(StringIO(poll_json))['poll']
        permanences_list = poll_dict['optionsText']
        familles_list = poll_dict['participants']
        # Is constrained?
        contraintes = False
        if 'columnConstraint' in poll_dict:
            contraintes = True
        if 'title' in poll_dict:
            print('Sondage : {}'.format(poll_dict['title']))
        if 'lastActivity' in poll_dict:
            print('Dernière activité : {}'.format(poll_dict['lastActivity']))

        # Liste des familles
        familles_noms = [f['name'] for f in familles_list]
        familles_souhaits = [f['preferences'] for f in familles_list]
        # Clean familles
        familles_noms = self.clean_name(familles_noms)
        print('Familles : {}'.format(', '.join(familles_noms)))

        # Liste des permanences
        permanences = []
        for p in permanences_list:
            ps = p.split(u' \u2013 ')
            date_str = ps[0]
            date_dt = dt.datetime.strptime(date_str, '%a %m/%d/%y %I:%M %p')
            hfin_str = ps[1]
            hfin_dt = dt.datetime.strptime(hfin_str, '%I:%M %p')
            date_dt_fin = dt.datetime.combine(date_dt.date(), hfin_dt.time())
            date_jour = date_dt.strftime('%Y-%m-%d')
            hdebut_str = date_dt.strftime('%Hh%M')
            hfin_str = hfin_dt.strftime('%Hh%M')
            date = '{}T{}-{}'.format(date_jour, hdebut_str, hfin_str)
            hdebut = float(date_dt.strftime('%H.%M'))
            hfin = float(hfin_dt.strftime('%H.%M'))
            # Si hfin = '##h01' ou '##h02', alors hfin = '##h00'
            if hfin - int(hfin) <= 0.02:
                hfin = round(hfin)
            duree_dt = date_dt_fin - date_dt
            duree = round(duree_dt.seconds / 3600., 1)
            jour = format_datetime(date_dt, 'EEEE', locale='fr')
            permanences.append({
                'date': date,            # nom unique de la permanence
                'duree': duree,          # duree en heures
                'date_jour': date_jour,  # '%Y-%m-%d'
                'hdebut': hdebut,         # float, 8.0, 8.30, ...
                'hfin': hfin,             # float, 9.0, 9.30, ...
                'jour': jour,            # nom du jour en francais
            })

        # Souhaits des familles
        souhaits = []
        weights = {
            'y': 1,    # ok
            'i': 0.5,  # peut-etre
            'n': 0,    # pas disponible
            'q': 0,    # '?': choix non renseigne
        }
        for i, nom in enumerate(familles_noms):
            for j, s in enumerate(familles_souhaits[i]):
                if weights[s]:
                    souhaits.append({
                        'nom': nom,
                        'date': permanences[j]['date'],
                        'souhait': weights[s],
                        'fixe': 0,
                    })

        self.familles_noms = familles_noms
        self.permanences = permanences
        self.souhaits = souhaits
        self.contraintes = contraintes


    def simulation(self):

        familles_df = self.familles_df
        permanences_df = self.permanences_df
        souhaits_df = self.souhaits_df
        simu_nsouhaits = self.simu_nsouhaits
        simu_garder = self.simu_garder

        import random
        sindex = souhaits_df.index
        perm_ids = permanences_df.index.tolist()
        for nom in familles_df.index:
            random.shuffle(perm_ids)
            i1, i2 = simu_nsouhaits
            cpt = random.randint(-3,3)
            i1 = max(0, i1 - cpt)
            familles_df.loc[nom]['compteur'] = cpt
            print('Simulation de souhaits pour {:<10} ({:>2}, {:>1})'.format(nom, i1, i2))
            for p in perm_ids[0:i1]:
                if souhaits_df.loc[souhaits_df['date'] == p].empty:
                    s = {'nom':nom, 'date':permanences_df.iloc[p]['date'], 'souhait':1, 'fixe':False}
                    souhaits_df.loc[souhaits_df.index.max() + 1] = s
            for p in perm_ids[i1:i1+i2]:
                if souhaits_df.loc[souhaits_df['date'] == p].empty:
                    s = {'nom':nom, 'date':permanences_df.iloc[p]['date'], 'souhait':0.5, 'fixe':False}
                    souhaits_df.loc[souhaits_df.index.max() + 1] = s
        if not simu_garder:
            souhaits_df = souhaits_df.drop(sindex)

        self.souhaits_df = souhaits_df


    def init_compteurs(self, p):
        print('Initialise les compteurs pour ' + p)
        fn = os.path.join(self.path, dat_path, p + '_compteurs.dat')
        with open(fn, 'r') as fp:
            self.compteurs.delete(1.0, END)
            self.compteurs.insert(END, fp.read())


    def compteurs_avant(self, datfile_compteurs):
        flist = glob.glob(os.path.join(self.dat_path, "*_souhaits.dat"))
        flist.sort()
        current_datfile_souhaits = os.path.join(self.dat_path, datfile_compteurs.replace('_compteurs.dat', '_souhaits.dat'))
        fid = 0
        if current_datfile_souhaits in flist:
            fid = flist.index(current_datfile_souhaits)
        familles_avant = {}
        if fid > 1:
            # cherche le fichier _ref_ précédent
            fn = flist[fid-1]
            if not '_ref_' in fn:
                if fid > 2:
                    fn = flist[fid-2]
            # cherche le bilan correspondant
            fn_bilan = fn.replace('dat/', 'Bilans/').replace('_souhaits.dat', '_bilan.txt')
            if os.path.isfile(fn_bilan):
                print('Bilan précédent trouvé : {}'.format(fn_bilan.split('/')[-1]))
                with open(fn_bilan, 'r') as fin:
                    # Lit les lignes avec les noms
                    keep = False
                    for line in fin.readlines():
                        if '-----' in line:
                            keep = False
                        if keep:
                            elts = line.split()
                            nom = elts[0]
                            cpt = elts[2]
                            autre = elts[4]
                            familles_avant[nom] = {
                                'compteur': cpt,    # compteur famille avant attribution
                                'coeff': 1,       # coefficient d'heure a attribuer
                                'hmin': 0,        # nombre minimum d'heures a attribuer
                                'hmax': self.hmax_mois,     # nombre maximum d'heures a attribuer
                                'avance': 0,      # permet de prendre des heures d'avance
                                'autre': autre,       # autres heures a comptabiliser (bureau, régularisation)
                            }
                        if 'avant' in line and 'apres' in line:
                            keep = True
                return familles_avant
            else:
                print('Pas de bilan précédent trouvé.')
        else:
            print('Pas de fichier souhaits trouvé.')
        return {}

    def lire_compteurs(self, datfile_compteurs):
        familles_df = pd.read_table(os.path.join(self.dat_path, datfile_compteurs), header=1, delim_whitespace=True, skiprows=[2], index_col=0, comment=';')
        del familles_df.index.name
        return familles_df


    def ecrire_compteurs(self, familles_df, datfile_compteurs):
        # print(str(familles_df.index))
        # print(str(familles_df))
        familles_header = [
            'table familles={nom} compteur(nom) coeff(nom) hmin(nom) hmax(nom) avance(nom) autre(nom):',
        ]
        familles_cols = ['compteur', 'coeff', 'hmin', 'hmax', 'avance', 'autre']
        familles_data0 = familles_df.to_string(columns=familles_cols)
        fsp = familles_data0.split('\n')
        fsp.insert(1, ':=')
        fsp[0] = 'nom' + fsp[0][3:]
        familles_data = '\n'.join(fsp)
        with open(os.path.join(self.dat_path, datfile_compteurs), 'w') as fout:
            fout.write('\n'.join(familles_header) + '\n')
            fout.write(familles_data)
            fout.write('\n;')


    def charger_souhaits(self):

        # Charger doodle
        print('-----------')
        self.load_doodle()

        familles_noms = self.familles_noms
        permanences = self.permanences
        souhaits = self.souhaits
        contraintes = self.contraintes

        # Ajout d'une famille '_Aprendre' pour perm non attribuées
        familles_noms.append('_Aprendre')
        for perm in permanences:
            souhaits.append({
                'date': perm['date'],
                'nom': '_Aprendre',
                'souhait': 1,
                'fixe': 0,
            })

        # Liste des familles par defaut
        familles = {}
        for nom in familles_noms:
            coeff = 1
            hmax = self.hmax_mois
            if nom == '_Aprendre':
                coeff = 0
                hmax = 500
            familles[nom] = {
                'compteur': 0,    # compteur famille avant attribution
                'coeff': coeff,   # coefficient d'heure a attribuer
                'hmin': 0,        # nombre minimum d'heures a attribuer
                'hmax': hmax,     # nombre maximum d'heures a attribuer
                'avance': 0,      # permet de prendre des heures d'avance
                'autre': 0,       # autres heures a comptabiliser (bureau, régularisation)
            }

        # DataFrames
        familles_df = pd.DataFrame.from_dict(familles, orient='index')
        permanences_df = pd.DataFrame.from_dict(permanences, orient='columns')
        souhaits_df = pd.DataFrame.from_dict(souhaits, orient='columns')

        # Mois du sondage
        d0 = permanences[0]['date_jour']
        d0_dt = dt.datetime.strptime(d0, '%Y-%m-%d')
        mois = d0_dt.strftime('%Y-%m')

        # Simulations
        if self.simu_active:
            mois = mois + '_simu'
            self.simulation()
        # Indique si le sondage est contraint (1 choix par colonne)
        # C'est alors un sondage de référence pour les attributions
        elif contraintes:
            mois = mois + '_ref'
        else:
            mois = mois + '_dispo'

        # Indicateur de permanences simultannées/consécutives
        perm_simult = []
        i_simult = 1
        date_jour_ref, hdebut_ref, hfin_ref =  '', 0., 0.
        for i in permanences_df.index.tolist():
            date_jour, heures = permanences_df.iloc[i]['date'].split('T')
            hdebut = permanences_df.iloc[i]['hdebut']
            hfin = permanences_df.iloc[i]['hfin']
            perm_simult.append(0)
            if (date_jour == date_jour_ref) and (hfin_ref > hdebut):
                if perm_simult[-2] == 0:
                    # Nouveau bloc de permanences simultannées
                    perm_simult[-2] = i_simult
                    perm_simult[-1] = i_simult
                    i_simult += 1
                else:
                    # Même bloc de permanences simultannées
                    perm_simult[-1] = perm_simult[-2]
                #print('! Permanences simultannées/consécutives le {} : {:05.2f}-{:05.2f} <> {:05.2f}-{:05.2f} ({})'.format(date_jour, hdebut_ref, hfin_ref, hdebut, hfin, perm_simult[-1]))
            date_jour_ref, hdebut_ref, hfin_ref = date_jour, hdebut, hfin
        permanences_df['simult'] = perm_simult

        # Fichiers de sortie
        datfile_prefix = 'perm_{}_{}'.format(mois, self.poll_id)
        datfile_compteurs = datfile_prefix + '_compteurs.dat'
        datfile_souhaits = datfile_prefix + '_souhaits.dat'
        datfile_bilan = datfile_prefix + '_bilan.txt'

        # Permanences et souhaits
        print('-----------')
        print('Création du fichier des souhaits : {}'.format(datfile_souhaits))
        # Liste des permanences
        permanences_header = [
            'table permanences={date} simult(date) hdebut(date) hfin(date) duree(date) jour(date) date_jour(date) :'
            'date  simult  hdebut  hfin  duree  jour  date_jour'
            ':=']
        permanences_cols = ['date', 'simult', 'hdebut', 'hfin', 'duree', 'jour', 'date_jour']
        permanences_data = permanences_df.to_string(columns=permanences_cols, index=False, header=False, justify='left')
        with open(os.path.join(self.dat_path, datfile_souhaits), 'w') as fout:
            fout.write('\n'.join(permanences_header) + '\n')
            fout.write(permanences_data)
            fout.write('\n;')
        # Souhaits des familles
        souhaits_header = [
            'table souhaits(nom,date) fixes(nom,date) :',
            'nom  date  souhaits  fixes',
            ':=']
        souhaits_cols = ['nom', 'date', 'souhait', 'fixe']
        souhaits_df = souhaits_df.sort_values(by=['nom', 'date'])
        souhaits_data = souhaits_df.to_string(columns=souhaits_cols, index=False, header=False, justify='left')
        with open(os.path.join(self.dat_path, datfile_souhaits), 'a') as fout:
            fout.write('\n'.join(souhaits_header) + '\n')
            fout.write(souhaits_data)
            fout.write('\n;')

        # Familles et compteurs
        if os.path.isfile(os.path.join(self.dat_path, datfile_compteurs)):
            # Le fichier existe déjà
            print('Fichier des compteurs familles existant et conservé.')
            #with open(os.path.join(self.dat_path, datfile_compteurs), 'r') as fin:
            #    print(fin.read())
        else:
            print('Création du fichier des compteurs familles...')
            # Init avec compteurs précédents et familles précédentes, si dispo
            familles_df = pd.DataFrame.from_dict(familles, orient='index')
            familles_avant = self.compteurs_avant(datfile_compteurs)
            if familles_avant:
                for nom in familles_avant:
                    if nom in familles:
                        familles[nom]['compteur'] = familles_avant[nom]['compteur']
                        print('Mise à jour du compteur pour {} : {}'.format(nom, familles_avant[nom]['compteur']))
                    else:
                        familles[nom] = familles_avant[nom]
                        print('Ajout de {} : {}'.format(nom, familles_avant[nom]))
                familles_df = pd.DataFrame.from_dict(familles, orient='index')
            # Création du fichier
            self.ecrire_compteurs(familles_df, datfile_compteurs)

        self.familles_df = familles_df
        self.permanences_df = permanences_df
        self.souhaits_df = souhaits_df

        # Affiche le dernier bilan
        #if os.path.isfile(os.path.join(self.path, 'bilans', datfile_bilan)):
        #    with open(os.path.join(self.path, 'bilans', datfile_bilan), 'r') as fin:
        #        print(fin.read())

        # Fin
        print('-----------')
        print('pour optimiser : python permopt.py {}'.format(datfile_prefix))
        print('-----------')
        return datfile_prefix


if __name__ == '__main__':
    cwd = os.path.abspath(os.path.dirname(__file__))  # os.getcwd()
    poll_id = 'd2magctkyqw9uff4'  # sondage exemple
    # Extraction de poll_id
    if len(sys.argv) > 1:
        poll_id = sys.argv[1].split('_')[-1].split('.')[0]
    permsouhaits = PermSouhaits(poll_id, cwd)
    permsouhaits.charger_souhaits()
